# Software Freedom School

![SFS Logo](img/clean.jpeg)

## Introduction Page

### Section Headings:

Information about the class, like goals, pre-reqs, and such.

### These will be displayed on the top right of the website:



### License:

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
