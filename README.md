## Welcome to the SFS Class Webpage Template!

This template is designed to help teachers create GitLab pages sites for your class!

### Modifying for your class:

##### First things first. Update the `mkdocs.yml` file.
  1. Update the site_url to end in the repo name of your class. Example: `https://sofreeus.gitlab.io/repo-name-here`
  1. (Optional) Update the site_name to the class name. Example: `"Class Name Here"`

### Second, build your intro page:

All of your pages are markdown files in the `./docs/` folder. This template includes several sample files; `intro.md`, `lab0.md`, `lab1.md`, and `lab2.md`. Images to be used in your files need to be in the `./docs/img/` folder. It is simple markdown just like every other class in the SFS [gitlab](www.gitlab.com/sofreeus) repos.

### Building the website:

Once complete, simply commit your changes and push them to the repo that you created from this template. The repo will automatically build the page and create it at sofreeus.gitlab.io/repo-Name

### Done!
